from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='home'),
    path('about', views.about, name='about-us'),
    path('log_in', views.log_in, name='sign-in'),
]